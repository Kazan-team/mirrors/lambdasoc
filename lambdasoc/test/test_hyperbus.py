# Test of HyperRAM class
#
# Copyright (c) 2019 Florent Kermarrec <florent@enjoy-digital.fr>
# Copyright (c) 2021 Luke Kenneth Casson Leighton <lkcl@lkcl.net>
#
# Based on code from Kermarrec, Licensed BSD-2-Clause
#
# Modifications for the Libre-SOC Project funded by NLnet and NGI POINTER
# under EU Grants 871528 and 957073, under the LGPLv3+ License

import unittest

from nmigen import (Record, Module, Signal, Elaboratable)
from nmigen.compat.sim import run_simulation

from lambdasoc.periph.hyperram import HyperRAM, HyperRAMPads, HyperRAMPHY

def c2bool(c):
    return {"-": 1, "_": 0}[c]


def wb_write(bus, addr, data, sel):
    yield bus.adr.eq(addr)
    yield bus.dat_w.eq(data)
    yield bus.sel.eq(sel)
    yield bus.we.eq(1)
    yield bus.cyc.eq(1)
    yield bus.stb.eq(1)
    yield
    while not (yield bus.ack):
        yield
    yield bus.cyc.eq(0)
    yield bus.stb.eq(0)

    
def wb_read(bus, addr, sel):
    yield bus.adr.eq(addr)
    yield bus.sel.eq(sel)
    yield bus.cyc.eq(1)
    yield bus.stb.eq(1)
    yield
    while not (yield bus.ack):
        yield
    yield bus.cyc.eq(0) 
    yield bus.stb.eq(0)

    return (yield bus.dat_r)

    
class TestHyperBusWrite(unittest.TestCase):

    def test_hyperram_write(self):
        def fpga_gen(dut):
            yield from wb_write(dut.bus, 0x1234, 0xdeadbeef, sel=0b1001)
            yield

        def hyperram_gen(dut):
            ck      = "___--__--__--__--__--__--__--__--__--__--__--__--__--__--__--__--_______"
            cs_n    = "--________________________________________________________________------"
            dq_oe   = "__------------____________________________________________--------______"
            dq_o    = "002000048d000000000000000000000000000000000000000000000000deadbeef000000"
            rwds_oe = "__________________________________________________________--------______"
            rwds_o  = "____________________________________________________________----________"

            for i in range(3):
                yield
            if False: # useful for printing out expected vs results
              for i in range(len(ck)):
                print ("i", i)
                print ("ck", c2bool(ck[i]), (yield dut.phy.pads.ck))
                print ("cs_n", c2bool(cs_n[i]), (yield dut.phy.pads.cs_n))
                print ("dq_oe", c2bool(dq_oe[i]), (yield dut.phy.pads.dq.oe))
                print ("dq_o", hex(int(dq_o[2*(i//2):2*(i//2)+2], 16)),
                                    hex((yield dut.phy.pads.dq.o)))
                print ("rwds_oe", c2bool(rwds_oe[i]),
                                    (yield dut.phy.pads.rwds.oe))
                print ("rwds_o", c2bool(rwds_o[i]),
                                    (yield dut.phy.pads.rwds.o))
                yield
            for i in range(len(ck)):
                self.assertEqual(c2bool(ck[i]), (yield dut.phy.pads.ck))
                self.assertEqual(c2bool(cs_n[i]), (yield dut.phy.pads.cs_n))
                self.assertEqual(c2bool(dq_oe[i]), (yield dut.phy.pads.dq.oe))
                self.assertEqual(int(dq_o[2*(i//2):2*(i//2)+2], 16),
                                    (yield dut.phy.pads.dq.o))
                self.assertEqual(c2bool(rwds_oe[i]),
                                    (yield dut.phy.pads.rwds.oe))
                self.assertEqual(c2bool(rwds_o[i]),
                                    (yield dut.phy.pads.rwds.o))
                yield

        dut = HyperRAM(io=HyperRAMPads(), phy_kls=HyperRAMPHY)
        run_simulation(dut, [fpga_gen(dut), hyperram_gen(dut)],
                            vcd_name="sim.vcd")



class TestHyperBusRead(unittest.TestCase):
    def test_hyperram_read(self):
        def fpga_gen(dut):
            dat = yield from wb_read(dut.bus, 0x1234, 0b1111)
            self.assertEqual(dat, 0xdeadbeef)
            dat = yield from wb_read(dut.bus, 0x1235, 0b1111)
            self.assertEqual(dat, 0xcafefade)
            yield
            yield
            yield
            yield
            yield

        def hyperram_gen(dut):
            ck     = "___--__--__--__--__--__--__--__--__--__--__--__--__--__--__--__--__--__--__--__--_"
            cs_n    = "--________________________________________________________________________________"
            dq_oe   = "__------------____________________________________________________________________"
            dq_o    = "00a000048d000000000000000000000000000000000000000000000000000000000000000000000000"
            dq_i    = "0000000000000000000000000000000000000000000000000000000000deadbeefcafefade00000000"
            rwds_oe = "__________________________________________________________________________________"

            for i in range(3):
                print ("prep", i)
                yield
            for i in range(len(ck)):
                print ("i", i)
                yield dut.phy.pads.dq.i.eq(int(dq_i[2*(i//2):2*(i//2)+2], 16))
                print ("ck", c2bool(ck[i]), (yield dut.phy.pads.ck))
                print ("cs_n", c2bool(cs_n[i]), (yield dut.phy.pads.cs_n))
                print ("dq_oe", c2bool(dq_oe[i]), (yield dut.phy.pads.dq.oe))
                print ("dq_o", hex(int(dq_o[2*(i//2):2*(i//2)+2], 16)),
                                    hex((yield dut.phy.pads.dq.o)))
                print ("rwds_oe", c2bool(rwds_oe[i]),
                                    (yield dut.phy.pads.rwds.oe))
                self.assertEqual(c2bool(ck[i]), (yield dut.phy.pads.ck))
                self.assertEqual(c2bool(cs_n[i]), (yield dut.phy.pads.cs_n))
                self.assertEqual(c2bool(dq_oe[i]), (yield dut.phy.pads.dq.oe))
                self.assertEqual(int(dq_o[2*(i//2):2*(i//2)+2], 16),
                            (yield dut.phy.pads.dq.o))
                self.assertEqual(c2bool(rwds_oe[i]),
                            (yield dut.phy.pads.rwds.oe))
                yield

        dut = HyperRAM(io=HyperRAMPads(), phy_kls=HyperRAMPHY)
        run_simulation(dut, [fpga_gen(dut), hyperram_gen(dut)],
                            vcd_name="rd_sim.vcd")

if __name__ == '__main__':
    unittest.main()
    #t = TestHyperBusRead()
    #t.test_hyperram_read()
    #t = TestHyperBusWrite()
    #t.test_hyperram_write()
